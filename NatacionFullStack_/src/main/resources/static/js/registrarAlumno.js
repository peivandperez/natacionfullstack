function AlumnosController(opcion) {
	$("#msg").hide();
	$("#msg").removeClass("alert-success").addClass("alert-danger");
	var token = $("meta[name='_csrf']").attr("content");
	
	switch(opcion){
	case "list":
		$.ajax({
			type : "post",
		    headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/alumnos/list",
			success : function(res) {
				$('#registrarTable').bootstrapTable('load', res);
				
				$('#registrarTable tbody').on('click', 'tr', function () {
					$("#rut").val($(this).find("td:eq(0)").text()); //revisar con bbdd como viene
					$("#nombre").val($(this).find("td:eq(1)").text());
					$("#apellido").val($(this).find("td:eq(2)").text());
					$("#mail").val($(this).find("td:eq(3)").text());
					$("#direccion").val($(this).find("td:eq(4)").text());
					$("#comuna").val($(this).find("td:eq(5)").text());
					$("#telefono").val($(this).find("td:eq(6)").text());
					$("#activo").val($(this).find("td:eq(7)").text());
			
					
				});
				
				
				$("#myModal").modal({show:true});
			},
			error : function() {
				$("#msg").show();
				$("#msg").html("Error en busqueda de alumnos.")
			}
		});       			
		break;
		
	case "listPostulante":
		$.ajax({
			type : "post",
		    headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/postulacion/list",
			success : function(res) {
				$('#verPostulante').bootstrapTable('load', res);
				
				$('#verPostulante tbody').on('click', 'tr', function () {
					$("#rut").val($(this).find("td:eq(0)").text()); //revisar con bbdd como viene
					$("#nombre").val($(this).find("td:eq(1)").text());
					$("#apellido").val($(this).find("td:eq(2)").text());
					$("#mail").val($(this).find("td:eq(3)").text());
					$("#direccion").val($(this).find("td:eq(4)").text());
					$("#comuna").val($(this).find("td:eq(5)").text());	
					$("#curso").val($(this).find("td:eq(6)").text());	
			
				});
				
				
				$("#myModal2").modal({show:true});
			},
			error : function() {
				$("#msg").show();
				$("#msg").html("Error en busqueda de postulantes.")
			}
		});       			
		break;
		
		
	case "get":
		$.ajax({
			type : "post",
		    headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/alumnos/get",
			data : "rut="+$("#rut").val(),
			success : function(res) {
				if (res == null || res == "") {
					$("#msg").show();
					$("#msg").html("No se encontraron registros.");
				} else {	
					$("#rut").val(res.rut);
					$("#nombre").val(res.nombre);
					$("#apellido").val(res.apellido);
					$("#mail").val(res.mail);
					$("#direccion").val(res.direccion);
					$("#comuna").val(res.comuna);
					$("#telefono").val(res.telefono);
					$("#activo").val(res.activo);
				
				}
			},
			error : function() {
				$("#msg").show();
				$("#msg").html("Error en busqueda.");
			}
		});       			
		break;
		
	case "insert":
		var json = 
			{
				'rut':$("#rut").val(),
				'nombre':$("#nombre").val(),
				'apellido':	$("#apellido").val(),
				'mail':	$("#mail").val(),
				'direccion':$("#direccion").val(),
				'comuna':$("#comuna").val(),
				'telefono':$("#telefono").val(),
				'activo':$("#activo").val()

			};
		 var postData = JSON.stringify(json);

    $.ajax({
		type : "post",
		headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
		url : "/alumnos/insert",
		data : postData,
		contentType : "application/json; charset=utf-8",
        dataType : "json",
		success : function(res) {
			if (res == 1) {
				$("#msg").removeClass("alert-danger").addClass("alert-success");
				$("#msg").show();
				$("#msg").html("Registro ingresado correctamente.");
			} else {
				$("#msg").show();
				$("#msg").html("No se pudo ingresar el registro.");
			}
		},
		error : function() {
			$("#msg").show();
			$("#msg").html("No se pudo ingresar el registro.");
		}
	});       	
			
	    break;
	    
	case "update":
		var json = 
			{
				'rut': $("#rut").val(),
				'nombre': $("#nombre").val(),
				'apellido': $("#apellido").val(),
				'mail': $("#mail").val(),
				'direccion': $("#direccion").val(),
				'comuna': $("#comuna").val(),
				'telefono': $("#telefono").val(),
				'activo': $("#activo").val()
			
			};

		var postData = JSON.stringify(json);

		$.ajax({
			type : "post",
		    headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/alumnos/update",
			data : postData,
			contentType : "application/json; charset=utf-8",
			dataType : "json",
			success : function(res) {
				if (res == 1) {
					$("#msg").removeClass("alert-danger").addClass("alert-success");
					$("#msg").show();
					$("#msg").html("Registro modificado correctamente.");
				} else {
					$("#msg").show();
					$("#msg").html("No se pudo modificar el registro.");
				}
			},
			error : function() {
				$("#msg").show();
				$("#msg").html("No se pudo modificar el registro.");
			}
		});       	
    break;
    
	case "delete":
		$.ajax({
			type : "post",
		    headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/alumnos/delete",
			data : "rut="+$("#rut").val(),
			success : function(res) {
				if (res == 1) {
					$("#msg").removeClass("alert-danger").addClass("alert-success");
					$("#msg").show();
					$("#msg").html("Registro eliminado correctamente.");
				} else {
					$("#msg").show();
					$("#msg").html("No se pudo eliminar el registro.");
				}
			},
			error : function() {
				$("#msg").show();
				$("#msg").html("No se pudo eliminar el registro.");
			}
		});
		break;
		
	case "deletePostulante":
		$.ajax({
			type : "post",
		    headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
			url : "/postulacion/delete",
			data : "rut="+$("#rut").val(),
			success : function(res) {
				if (res == 1) {
					$("#msg").removeClass("alert-danger").addClass("alert-success");
					$("#msg").show();
					$("#msg").html("Registro eliminado correctamente.");
				} else {
					$("#msg").show();
					$("#msg").html("No se pudo eliminar el registro.");
				}
			},
			error : function() {
				$("#msg").show();
				$("#msg").html("No se pudo eliminar el registro.");
			}
		});
		break;

		
		
	default:
		$("#msg").show();
		$("#msg").html("Opción incorrecta.");
	}
}
