function PostulacionController(opcion) {
	$("#msg").hide();
	$("#msg").removeClass("alert-success").addClass("alert-danger");
	var token = $("meta[name='_csrf']").attr("content");
	
switch(opcion){
	case "insert":
	var json = 
		{
			'rut': $("#rut").val(),
			'nombre': $("#nombre").val(),
			'apellido': $("#apellido").val(),
			'mail': $("#mail").val(),
			'direccion': $("#direccion").val(),
			'comuna': $("#comuna").val(),
			'id_curso': $("#id_curso").val()
			
		};

    var postData = JSON.stringify(json);

    $.ajax({
		type : "post",
		headers: {"X-CSRF-TOKEN": token}, //send CSRF token in header
		url : "/postulacion/insert",
		data : postData,
		contentType : "application/json; charset=utf-8",
        dataType : "json",
		success : function(res) {
			if (res == 1) {
				$("#msg").removeClass("alert-danger").addClass("alert-success");
				$("#msg").show();
				$("#msg").html("Registro ingresado correctamente.");
			} else {
				$("#msg").show();
				$("#msg").html("No se pudo ingresar el registro.");
			}
		},
		error : function() {
			$("#msg").show();
			$("#msg").html("No se pudo ingresar el registro.");
		}
	});       	
    break;


}
}