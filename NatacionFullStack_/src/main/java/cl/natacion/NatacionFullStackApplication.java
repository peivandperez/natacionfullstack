package cl.natacion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NatacionFullStackApplication {

	public static void main(String[] args) {
		SpringApplication.run(NatacionFullStackApplication.class, args);
	}

}
