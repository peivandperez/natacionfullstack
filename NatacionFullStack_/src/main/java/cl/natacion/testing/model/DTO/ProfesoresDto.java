package cl.natacion.testing.model.DTO;

public class ProfesoresDto {
 int id_profesor;
 String nombre;
 String apellido;
 String mail;
 String rut;
 
 
 
 
@Override
public String toString() {
	return "ProfesoresDto [id_profesor=" + id_profesor + ", nombre=" + nombre + ", apellido=" + apellido + ", mail="
			+ mail + ", rut=" + rut + "]";
}
public int getId_profesor() {
	return id_profesor;
}
public void setId_profesor(int id_profesor) {
	this.id_profesor = id_profesor;
}
public String getNombre() {
	return nombre;
}
public void setNombre(String nombre) {
	this.nombre = nombre;
}
public String getApellido() {
	return apellido;
}
public void setApellido(String apellido) {
	this.apellido = apellido;
}
public String getMail() {
	return mail;
}
public void setMail(String mail) {
	this.mail = mail;
}
public String getRut() {
	return rut;
}
public void setRut(String rut) {
	this.rut = rut;
}
public ProfesoresDto(int id_profesor, String nombre, String apellido, String mail, String rut) {
	this.id_profesor = id_profesor;
	this.nombre = nombre;
	this.apellido = apellido;
	this.mail = mail;
	this.rut = rut;
}
public ProfesoresDto() {
}
 
 
}
