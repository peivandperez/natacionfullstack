package cl.natacion.testing.model.DAO;

import java.util.List;

import cl.natacion.testing.model.DTO.ClasesDto;

public interface ClasesDao {
	public int insert(ClasesDto clasesDto); //Es tipo int debido a que la operación devuelve la cantidad de filas modificadas, en este caso queremos realizar sólo una modificacion. Recibe un dato tipo SalesDTO con nombre SalesDTO
	public ClasesDto get(int id_clase); //como es un read, vamos a obtener un dato de SalesDTO, un sólo sale y lo manejamos a nivel DTO,
	public int update(ClasesDto clasesDto); //recibe un dato tipo SalesDTO con nombre SalesDTO
	public int delete(int id_clase);//
	public List<ClasesDto> listClases();// una lista de tipo SalesDTO llamado list
}
