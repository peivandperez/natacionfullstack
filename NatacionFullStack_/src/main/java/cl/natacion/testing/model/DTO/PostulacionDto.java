package cl.natacion.testing.model.DTO;

public class PostulacionDto {

	 String rut;   
	 String nombre;
	 String apellido; 
	 String mail;  
	 String direccion; 
	 String comuna;
	 int id_curso;
	 
	 
	@Override
	public String toString() {
		return "PostulacionDto [rut=" + rut + ", nombre=" + nombre + ", apellido=" + apellido + ", mail=" + mail
				+ ", direccion=" + direccion + ", comuna=" + comuna + ", id_curso=" + id_curso + "]";
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public int getId_curso() {
		return id_curso;
	}
	public void setId_curso(int id_curso) {
		this.id_curso = id_curso;
	}
	public PostulacionDto(String rut, String nombre, String apellido, String mail, String direccion, String comuna,
			int id_curso) {
		this.rut = rut;
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.direccion = direccion;
		this.comuna = comuna;
		this.id_curso = id_curso;
	}
	public PostulacionDto() {
	}

	 

	 
}

