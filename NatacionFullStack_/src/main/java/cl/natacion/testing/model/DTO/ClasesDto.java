package cl.natacion.testing.model.DTO;

import java.sql.Date;

public class ClasesDto {
 int id_clase;
 int id_piscina;
 int id_profesor;
 Date fecha;
 //
 
@Override
public String toString() {
	return "ClasesDto [id_clase=" + id_clase + ", id_piscina=" + id_piscina + ", id_profesor=" + id_profesor
			+ ", fecha=" + fecha + "]";
}
public int getId_clase() {
	return id_clase;
}
public void setId_clase(int id_clase) {
	this.id_clase = id_clase;
}
public int getId_piscina() {
	return id_piscina;
}
public void setId_piscina(int id_piscina) {
	this.id_piscina = id_piscina;
}
public int getId_profesor() {
	return id_profesor;
}
public void setId_profesor(int id_profesor) {
	this.id_profesor = id_profesor;
}
public Date getFecha() {
	return fecha;
}
public void setFecha(Date fecha) {
	this.fecha = fecha;
}
public ClasesDto(int id_clase, int id_piscina, int id_profesor, Date fecha) {
	this.id_clase = id_clase;
	this.id_piscina = id_piscina;
	this.id_profesor = id_profesor;
	this.fecha = fecha;
}
public ClasesDto() {
}
}
