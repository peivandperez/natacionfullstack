package cl.natacion.testing.model.DAO;

import java.util.List;

import cl.natacion.testing.model.DTO.ProfesoresDto;



public interface ProfesoresDao {
	public int insert(ProfesoresDto profesoresDto); //Es tipo int debido a que la operación devuelve la cantidad de filas modificadas, en este caso queremos realizar sólo una modificacion. Recibe un dato tipo SalesDTO con nombre SalesDTO
	public ProfesoresDto get(int id_profesor); //como es un read, vamos a obtener un dato de SalesDTO, un sólo sale y lo manejamos a nivel DTO,
	public int update(ProfesoresDto profesoresDto); //recibe un dato tipo SalesDTO con nombre SalesDTO
	public int delete(int id_profesor);//
	public List<ProfesoresDto> listProfesores();// una lista de tipo SalesDTO llamado list
}
