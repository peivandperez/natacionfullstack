package cl.natacion.testing.model.DAO;

import java.util.List;

import cl.natacion.testing.model.DTO.AlumnosDto;



public interface AlumnosDao {
	public int insert(AlumnosDto alumnosDto); //Es tipo int debido a que la operación devuelve la cantidad de filas modificadas, en este caso queremos realizar sólo una modificacion. Recibe un dato tipo SalesDTO con nombre SalesDTO
	public AlumnosDto get(String rut); //como es un read, vamos a obtener un dato de SalesDTO, un sólo sale y lo manejamos a nivel DTO,
	public int update(AlumnosDto alumnosDto); //recibe un dato tipo SalesDTO con nombre SalesDTO
	public int delete(String rut);//
	public List<AlumnosDto> listAlumnos();// una lista de tipo SalesDTO llamado list
}
