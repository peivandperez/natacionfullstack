package cl.natacion.testing.model.DTO;

public class UsersRolesDto {
	int id_users;
	int id_roles;
	int id_users_roles;
	String username;
	String password;
	String enabled; //'Y' para true y 'N' para false. por default es true
	String role;
	
	
	
	@Override
	public String toString() {
		return "UsersRolesDto [id_users=" + id_users + ", id_roles=" + id_roles + ", id_users_roles=" + id_users_roles
				+ ", username=" + username + ", password=" + password + ", enabled=" + enabled + ", role=" + role + "]";
	}
	public int getId_users() {
		return id_users;
	}
	public void setId_users(int id_users) {
		this.id_users = id_users;
	}
	public int getId_roles() {
		return id_roles;
	}
	public void setId_roles(int id_roles) {
		this.id_roles = id_roles;
	}
	public int getId_users_roles() {
		return id_users_roles;
	}
	public void setId_users_roles(int id_users_roles) {
		this.id_users_roles = id_users_roles;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEnabled() {
		return enabled;
	}
	public void setEnabled(String enabled) {
		this.enabled = enabled;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public UsersRolesDto(int id_users, int id_roles, int id_users_roles, String username, String password,
			String enabled, String role) {
		this.id_users = id_users;
		this.id_roles = id_roles;
		this.id_users_roles = id_users_roles;
		this.username = username;
		this.password = password;
		this.enabled = enabled;
		this.role = role;
	}
	public UsersRolesDto() {
	}
	
	
	
	
	

	
}
