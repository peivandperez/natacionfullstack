package cl.natacion.testing.model.DAO;


import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.natacion.testing.model.DTO.ProfesoresDto;
@Repository
public class ProfesoresDaoImpl implements ProfesoresDao {

	String insertProfesores ="INSERT INTO profesor (id_profesor,nombre,apellido,mail,rut) VALUES(:id_alumno, :nombre, :apellido, :mail, :rut)";
	String selectProfesores ="SELECT * FROM profesor where id_profesor=?";
	String updateProfesores ="UPDATE profesor SET id_profesor= :id_profesor, nombre= :nombre, apellido= :apellido, mail= :mail WHERE id_profesor= :id_profesor";
	String deleteProfesores=" DELETE FROM profesor where id_profesor=?";
	String listProfesoresSQL="SELECT * FROM profesor";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@Override
	public int insert(ProfesoresDto profesoresDto) {
		int rows =0;
		
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("id_profesor",profesoresDto.getId_profesor(),Types.INTEGER);
			params.addValue("nombre", profesoresDto.getNombre(), Types.VARCHAR);
			params.addValue("apellido", profesoresDto.getApellido(), Types.VARCHAR);
			params.addValue("mail", profesoresDto.getMail(), Types.VARCHAR);
			params.addValue("rut", profesoresDto.getRut(), Types.VARCHAR);
		
		
			rows = namedParameterJdbcTemplate.update(insertProfesores, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public ProfesoresDto get(int id_profesor) {
		Object args[]= {id_profesor};
		ProfesoresDto profesoresDto;
		try {
			profesoresDto= jdbcTemplate.queryForObject(selectProfesores, args, BeanPropertyRowMapper.newInstance(ProfesoresDto.class));
			
		} catch (Exception e) {
			profesoresDto=null;
			e.printStackTrace();
		}
		return profesoresDto; 
	}

	@Override
	public int update(ProfesoresDto profesoresDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("id_profesor",profesoresDto.getId_profesor(), Types.INTEGER);
			params.addValue("nombre", profesoresDto.getNombre(), Types.VARCHAR);
			params.addValue("apellido", profesoresDto.getApellido(), Types.VARCHAR);
			params.addValue("mail", profesoresDto.getMail(), Types.VARCHAR);
			params.addValue("rut", profesoresDto.getRut(), Types.VARCHAR);
			
			
		
			rows = namedParameterJdbcTemplate.update(updateProfesores, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public int delete(int id_profesor) {
		int rows=0;
		
		Object args[]= {id_profesor};
	try {
			rows= jdbcTemplate.update(deleteProfesores,args);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows; 
	}

	@Override
	public List<ProfesoresDto> listProfesores() {
		//devuelve un tipo List ProfesoresDto
		//vamos a hacer un mapeo para la fila. Se ocupa el rowMapper -->se ocupa clase BeanPropertyRowMapper, convierte una clase a un objeto, así
		//jdbcTemplate sabe que tiene que devolver un dato tipo ProfesoresDto
		//como esta consulta no recibe argumentos dado que es un Select * ; por lo tanto se quita el "args".	
			List<ProfesoresDto> listProfesores= jdbcTemplate.query(listProfesoresSQL, BeanPropertyRowMapper.newInstance(ProfesoresDto.class));
			return listProfesores;

}
}
