package cl.natacion.testing.model.DAO;

import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.natacion.testing.model.DTO.ClasesDto;


@Repository
public class ClasesDaoImpl implements ClasesDao {
	String insertClases ="INSERT INTO clase (id_clase,id_piscina,id_profesor,fecha) VALUES(:id_clase, :id_piscina, :id_profesor, :fecha)";
	String selectClases ="SELECT * FROM clase where id_clase=?";
	String updateClases ="UPDATE clase SET id_clase= :id_clase, id_piscina= :id_piscina, id_profesor= :id_profesor, fecha= :fecha WHERE id_clase= :id_clase";
	String deleteClases=" DELETE FROM clase where id_clase=?";
	String listClasesSQL="SELECT * FROM clase";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@Override
	public int insert(ClasesDto clasesDto) {
		int rows =0;
		
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("id_clase",clasesDto.getId_clase(),Types.INTEGER);
			params.addValue("id_piscina",clasesDto.getId_piscina(), Types.INTEGER);
			params.addValue("id_profesor",clasesDto.getId_profesor(),Types.INTEGER);
			params.addValue("fecha",clasesDto.getFecha(), Types.DATE);
		
		
		
			rows = namedParameterJdbcTemplate.update(insertClases, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public ClasesDto get(int id_clase) {
		Object args[]= {id_clase};
		ClasesDto clasesDto;
		try {
			clasesDto= jdbcTemplate.queryForObject(selectClases, args, BeanPropertyRowMapper.newInstance(ClasesDto.class));
			
		} catch (Exception e) {
			clasesDto=null;
			e.printStackTrace();
		}
		return clasesDto; 
	}

	@Override
	public int update(ClasesDto clasesDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("id_clase",clasesDto.getId_clase(),Types.INTEGER);
			params.addValue("id_piscina",clasesDto.getId_piscina(), Types.INTEGER);
			params.addValue("id_profesor",clasesDto.getId_profesor(),Types.INTEGER);
			params.addValue("fecha",clasesDto.getFecha(), Types.DATE);
			
			
		
			rows = namedParameterJdbcTemplate.update(updateClases, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public int delete(int id_clase) {
		int rows=0;
		
		Object args[]= {id_clase};
	try {
			rows= jdbcTemplate.update(deleteClases,args);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows; 
	}

	@Override
	public List<ClasesDto> listClases() {
		//devuelve un tipo List ClasesDto
		//vamos a hacer un mapeo para la fila. Se ocupa el rowMapper -->se ocupa clase BeanPropertyRowMapper, convierte una clase a un objeto, así
		//jdbcTemplate sabe que tiene que devolver un dato tipo ClasesDto
		//como esta consulta no recibe argumentos dado que es un Select * ; por lo tanto se quita el "args".	
			List<ClasesDto> listClases= jdbcTemplate.query(listClasesSQL, BeanPropertyRowMapper.newInstance(ClasesDto.class));
			return listClases;

}
}
