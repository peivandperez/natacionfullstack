package cl.natacion.testing.model.DAO;

import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.natacion.testing.model.DTO.CursosDto;


@Repository
public class CursosDaoImpl implements CursosDao {

	String insertCursos ="INSERT INTO cursos (id_curso,nombre_curso,veces_semana,nivel_nado,valor_mensual,activo) VALUES(:id_curso, :nombre_curso, :veces_semana, :nivel_nado, :valor_mensual, :activo)";
	String selectCursos ="SELECT * FROM cursos where id_curso=?";
	String updateCursos ="UPDATE cursos SET id_curso= :id_curso, nombre_curso= :nombre_curso, veces_semana= :veces_semana, nivel_nado= :nivel_nado, valor_mensual= :valor_mensual, activo= :activo WHERE id_curso= :id_curso";
	String deleteCursos=" DELETE FROM cursos where id_curso=?";
	String listCursosSQL="SELECT * FROM cursos";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@Override
	public int insert(CursosDto cursosDto) {
		int rows =0;
		
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("id_curso",cursosDto.getId_curso(),Types.INTEGER);
			params.addValue("nombre_curso",cursosDto.getNombre_curso(), Types.VARCHAR);
			params.addValue("veces_semana",cursosDto.getVeces_semana(), Types.INTEGER);
			params.addValue("nivel_nado",cursosDto.getNivel_nado(), Types.VARCHAR);
			params.addValue("valor_mensual",cursosDto.getValor_mensual(), Types.INTEGER);
			params.addValue("activo",cursosDto.getActivo(), Types.VARCHAR);
		
		
			rows = namedParameterJdbcTemplate.update(insertCursos, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public CursosDto get(int id_curso) {
		Object args[]= {id_curso};
		CursosDto cursosDto;
		try {
			cursosDto= jdbcTemplate.queryForObject(selectCursos, args, BeanPropertyRowMapper.newInstance(CursosDto.class));
			
		} catch (Exception e) {
			cursosDto=null;
			e.printStackTrace();
		}
		return cursosDto; 
	}

	@Override
	public int update(CursosDto cursosDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("id_curso",cursosDto.getId_curso(),Types.INTEGER);
			params.addValue("nombre_curso",cursosDto.getNombre_curso(), Types.VARCHAR);
			params.addValue("veces_semana",cursosDto.getVeces_semana(), Types.INTEGER);
			params.addValue("nivel_nado",cursosDto.getNivel_nado(), Types.VARCHAR);
			params.addValue("valor_mensual",cursosDto.getValor_mensual(), Types.INTEGER);
			params.addValue("activo",cursosDto.getActivo(), Types.VARCHAR);
			
			
		
			rows = namedParameterJdbcTemplate.update(updateCursos, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public int delete(int id_curso) {
		int rows=0;
		
		Object args[]= {id_curso};
	try {
			rows= jdbcTemplate.update(deleteCursos,args);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows; 
	}

	@Override
	public List<CursosDto> listCursos() {
		//devuelve un tipo List CursosDto
		//vamos a hacer un mapeo para la fila. Se ocupa el rowMapper -->se ocupa clase BeanPropertyRowMapper, convierte una clase a un objeto, así
		//jdbcTemplate sabe que tiene que devolver un dato tipo CursosDto
		//como esta consulta no recibe argumentos dado que es un Select * ; por lo tanto se quita el "args".	
			List<CursosDto> listCursos= jdbcTemplate.query(listCursosSQL, BeanPropertyRowMapper.newInstance(CursosDto.class));
			return listCursos;

}
}
