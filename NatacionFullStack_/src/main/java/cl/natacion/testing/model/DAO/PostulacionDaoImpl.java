package cl.natacion.testing.model.DAO;

import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.natacion.testing.model.DTO.PostulacionDto;


@Repository
public class PostulacionDaoImpl implements PostulacionDao {


	String insertPostulacion ="INSERT INTO postulacion (rut,nombre,apellido,mail,direccion,comuna,id_curso) VALUES(:rut, :nombre, :apellido, :mail, :direccion, :comuna, :id_curso)";
	String selectPostulacion ="SELECT * FROM postulacion where rut=?";
	String updatePostulacion ="UPDATE postulacion SET rut= :rut, nombre= :nombre, apellido= :apellido, mail= :mail, direccion= :direccion, comuna= :comuna, id_curso= :id_curso WHERE rut= :rut";
	String deletePostulacion=" DELETE FROM postulacion where rut=?";
	String listPostulacionSQL="SELECT * FROM postulacion";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@Override
	public int insert(PostulacionDto postulacionDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("rut",postulacionDto.getRut(), Types.VARCHAR);
			params.addValue("nombre", postulacionDto.getNombre(), Types.VARCHAR);
			params.addValue("apellido", postulacionDto.getApellido(), Types.VARCHAR);
			params.addValue("mail", postulacionDto.getMail(), Types.VARCHAR);
			params.addValue("rut", postulacionDto.getRut(), Types.VARCHAR);
			params.addValue("direccion", postulacionDto.getDireccion(), Types.VARCHAR);
			params.addValue("comuna", postulacionDto.getComuna(), Types.VARCHAR);
			params.addValue("id_curso", postulacionDto.getId_curso(), Types.INTEGER);
		
			rows = namedParameterJdbcTemplate.update(insertPostulacion, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public PostulacionDto get(String rut) {
		Object args[]= {rut};
		PostulacionDto postulacionDto;
		try {
			postulacionDto= jdbcTemplate.queryForObject(selectPostulacion, args, BeanPropertyRowMapper.newInstance(PostulacionDto.class));
			
		} catch (Exception e) {
			postulacionDto=null;
			e.printStackTrace();
		}
		return postulacionDto; 
	}

	@Override
	public int update(PostulacionDto postulacionDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("rut",postulacionDto.getRut(), Types.VARCHAR);
			params.addValue("nombre", postulacionDto.getNombre(), Types.VARCHAR);
			params.addValue("apellido", postulacionDto.getApellido(), Types.VARCHAR);
			params.addValue("mail", postulacionDto.getMail(), Types.VARCHAR);
			params.addValue("rut", postulacionDto.getRut(), Types.VARCHAR);
			params.addValue("direccion", postulacionDto.getDireccion(), Types.VARCHAR);
			params.addValue("comuna", postulacionDto.getComuna(), Types.VARCHAR);
			params.addValue("id_curso", postulacionDto.getId_curso(), Types.INTEGER);
			
			
		
			rows = namedParameterJdbcTemplate.update(updatePostulacion, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public int delete(String rut) {
		int rows=0;
		
		Object args[]= {rut};
	try {
			rows= jdbcTemplate.update(deletePostulacion,args);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows; 
	}

	@Override
	public List<PostulacionDto> listPostulacion() {
		//devuelve un tipo List PostulacionsDto
		//vamos a hacer un mapeo para la fila. Se ocupa el rowMapper -->se ocupa clase BeanPropertyRowMapper, convierte una clase a un objeto, así
		//jdbcTemplate sabe que tiene que devolver un dato tipo PostulacionsDto
		//como esta consulta no recibe argumentos dado que es un Select * ; por lo tanto se quita el "args".	
			List<PostulacionDto> listPostulacion= jdbcTemplate.query(listPostulacionSQL,BeanPropertyRowMapper.newInstance(PostulacionDto.class));
			return listPostulacion;	
	
}
}
