package cl.natacion.testing.model.DTO;

public class CursosDto {
	int id_curso;
	String nombre_curso;
	int veces_semana;
	String nivel_nado;
	int valor_mensual;
	String activo; //'Y' es activo - 'N' es inactivo
	
	
	@Override
	public String toString() {
		return "CursosDto [id_curso=" + id_curso + ", nombre_curso=" + nombre_curso + ", veces_semana=" + veces_semana
				+ ", nivel_nado=" + nivel_nado + ", valor_mensual=" + valor_mensual + ", activo=" + activo + "]";
	}
	public int getId_curso() {
		return id_curso;
	}
	public void setId_curso(int id_curso) {
		this.id_curso = id_curso;
	}
	public String getNombre_curso() {
		return nombre_curso;
	}
	public void setNombre_curso(String nombre_curso) {
		this.nombre_curso = nombre_curso;
	}
	public int getVeces_semana() {
		return veces_semana;
	}
	public void setVeces_semana(int veces_semana) {
		this.veces_semana = veces_semana;
	}
	public String getNivel_nado() {
		return nivel_nado;
	}
	public void setNivel_nado(String nivel_nado) {
		this.nivel_nado = nivel_nado;
	}
	public int getValor_mensual() {
		return valor_mensual;
	}
	public void setValor_mensual(int valor_mensual) {
		this.valor_mensual = valor_mensual;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	public CursosDto(int id_curso, String nombre_curso, int veces_semana, String nivel_nado, int valor_mensual,
			String activo) {
		this.id_curso = id_curso;
		this.nombre_curso = nombre_curso;
		this.veces_semana = veces_semana;
		this.nivel_nado = nivel_nado;
		this.valor_mensual = valor_mensual;
		this.activo = activo;
	}
	public CursosDto() {
	}
}
