package cl.natacion.testing.model.DAO;

import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.natacion.testing.model.DTO.AlumnosDto;

@Repository
public class AlumnosDaoImpl implements AlumnosDao {
	String insertAlumno ="INSERT INTO alumnos (nombre,apellido,mail,rut,activo,direccion,telefono,comuna) VALUES(:nombre, :apellido, :mail, :rut, :activo, :direccion, :telefono, :comuna)";
	String selectAlumno ="SELECT * FROM alumnos where rut=?";
	String updateAlumno ="UPDATE alumnos SET nombre= :nombre, apellido= :apellido, mail= :mail, direccion= :direccion, telefono= :telefono, comuna= :comuna WHERE rut= :rut";
	String deleteAlumno=" DELETE FROM alumnos where rut=?";
	String listAlumnosSQL="SELECT * FROM alumnos";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@Override
	public int insert(AlumnosDto alumnosDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
	
			params.addValue("nombre", alumnosDto.getNombre(),Types.VARCHAR);
			params.addValue("apellido", alumnosDto.getApellido(),Types.VARCHAR);
			params.addValue("mail", alumnosDto.getMail(),Types.VARCHAR);
			params.addValue("rut", alumnosDto.getRut(),Types.VARCHAR);
			params.addValue("activo", alumnosDto.getActivo(),Types.VARCHAR);
			params.addValue("direccion", alumnosDto.getDireccion(),Types.VARCHAR);
			params.addValue("telefono", alumnosDto.getTelefono(),Types.VARCHAR);
			params.addValue("comuna", alumnosDto.getComuna(),Types.VARCHAR);
			
		
			rows = namedParameterJdbcTemplate.update(insertAlumno, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public AlumnosDto get(String rut) {
		Object args[]= {rut};
		AlumnosDto alumnosDto;
		try {
			alumnosDto= jdbcTemplate.queryForObject(selectAlumno, args, BeanPropertyRowMapper.newInstance(AlumnosDto.class));
			
		} catch (Exception e) {
			alumnosDto=null;
			e.printStackTrace();
		}
		return alumnosDto; 
	}

	@Override
	public int update(AlumnosDto alumnosDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		

			params.addValue("nombre", alumnosDto.getNombre(), Types.VARCHAR);
			params.addValue("apellido", alumnosDto.getApellido(), Types.VARCHAR);
			params.addValue("mail", alumnosDto.getMail(), Types.VARCHAR);
			params.addValue("rut", alumnosDto.getRut(), Types.VARCHAR);
			params.addValue("activo", alumnosDto.getActivo(), Types.VARCHAR);
			params.addValue("direccion", alumnosDto.getDireccion(), Types.VARCHAR);
			params.addValue("telefono", alumnosDto.getTelefono(), Types.VARCHAR);
			params.addValue("comuna", alumnosDto.getComuna(), Types.VARCHAR);
			
			
		
			rows = namedParameterJdbcTemplate.update(updateAlumno, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public int delete(String rut) {
		int rows=0;
		
		Object args[]= {rut};
	try {
			rows= jdbcTemplate.update(deleteAlumno,args);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows; 
	}

	@Override
	public List<AlumnosDto> listAlumnos() {
		//devuelve un tipo List AlumnosDto
		//vamos a hacer un mapeo para la fila. Se ocupa el rowMapper -->se ocupa clase BeanPropertyRowMapper, convierte una clase a un objeto, así
		//jdbcTemplate sabe que tiene que devolver un dato tipo AlumnosDto
		//como esta consulta no recibe argumentos dado que es un Select * ; por lo tanto se quita el "args".	
			List<AlumnosDto> listAlumnos= jdbcTemplate.query(listAlumnosSQL, BeanPropertyRowMapper.newInstance(AlumnosDto.class));
			return listAlumnos;	
	}

}
