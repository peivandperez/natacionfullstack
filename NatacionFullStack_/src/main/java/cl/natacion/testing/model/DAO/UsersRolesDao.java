package cl.natacion.testing.model.DAO;

import java.util.List;

import cl.natacion.testing.model.DTO.UsersRolesDto;

public interface UsersRolesDao {
	public int insert(UsersRolesDto userRolesDto); //Es tipo int debido a que la operación devuelve la cantidad de filas modificadas, en este caso queremos realizar sólo una modificacion. Recibe un dato tipo SalesDTO con nombre SalesDTO
	public UsersRolesDto get(int id_users); //como es un read, vamos a obtener un dato de SalesDTO, un sólo sale y lo manejamos a nivel DTO,
	public int update(UsersRolesDto userRolesDto); //recibe un dato tipo SalesDTO con nombre SalesDTO
	public int delete(int id_users);//
	public List<UsersRolesDto> listUserRoles();// una lista de tipo SalesDTO llamado list
	
	public int insertRoles(UsersRolesDto userRolesDto);
	public int updateRoles(UsersRolesDto userRolesDto);
}
