package cl.natacion.testing.model.DAO;

import java.sql.Types;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import cl.natacion.testing.model.DTO.UsersRolesDto;


@Repository
public class UsersRolesDaoImpl implements UsersRolesDao {
	
	String insertUsersRoles ="INSERT INTO users (username,password,enabled) VALUES(:username, :password, :enabled)";
	String insertUsersRolesAux="Insert INTO user_roles (id_users,id_roles) VALUES (:id_users, :id_roles)";
	String selectUsersRoles ="SELECT u.username, r.role FROM users u, rolres r, users_roles ur WHERE :ur.id_users = :id_users and r.id_roles = ur.id_roles";
	String updateUsers="UPDATE users SET username= :username, enabled= :enabled WHERE id_users= :id_users";
	String updateRoles="UPDATE users_roles SET id_roles= :id_roles where id_users= :id_users ";
	String deleteUsersRoles=" DELETE FROM users where id_users=?";
	
	String listUsersRolesSQL="SELECT u.username, r.role from users u, roles r, users_roles ur where u.id_users=ur.id_users and r.id_roles = ur.id_roles";
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Autowired 
	NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	
	@Override
	public int insert(UsersRolesDto userRolesDto) {
		int rows =0;
		
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("username", userRolesDto.getUsername(),Types.VARCHAR);
			params.addValue("password", userRolesDto.getPassword(), Types.VARCHAR);
			params.addValue("enabled", "Y", Types.VARCHAR);
		
			rows = namedParameterJdbcTemplate.update(insertUsersRoles, params);
		
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public int insertRoles(UsersRolesDto userRolesDto) {
		int rows =0;
		
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		

			params.addValue("id_users", userRolesDto.getId_users(),Types.INTEGER);
			params.addValue("id_roles", userRolesDto.getId_roles(), Types.INTEGER);
					
			rows = namedParameterJdbcTemplate.update(insertUsersRolesAux, params);
		
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}
	@Override
	public UsersRolesDto get(int id_users) {
		Object args[]= {id_users};
		UsersRolesDto userRolesDto;
		try {
			userRolesDto= jdbcTemplate.queryForObject(selectUsersRoles, args, BeanPropertyRowMapper.newInstance(UsersRolesDto.class));
			
		} catch (Exception e) {
			userRolesDto=null;
			e.printStackTrace();
		}
		return userRolesDto; 
	}

	@Override
	public int update(UsersRolesDto userRolesDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("username",userRolesDto.getUsername(),Types.VARCHAR);
			params.addValue("enabled",userRolesDto.getEnabled(),Types.VARCHAR);
			params.addValue("id_users",userRolesDto.getId_users(),Types.INTEGER);
			
			rows = namedParameterJdbcTemplate.update(updateUsers, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}
	
	@Override
	public int updateRoles(UsersRolesDto userRolesDto) {
		int rows =0;
		
		 
		try {
	
			MapSqlParameterSource params =new MapSqlParameterSource();
		
			params.addValue("id_roles",userRolesDto.getId_roles(),Types.INTEGER);	
			params.addValue("id_users",userRolesDto.getId_users(),Types.INTEGER);
			
			rows = namedParameterJdbcTemplate.update(updateRoles, params);
			
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		
		return rows;
	}

	@Override
	public int delete(int id_users) {
		int rows=0;
		
		Object args[]= {id_users};
	try {
			rows= jdbcTemplate.update(deleteUsersRoles,args);
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows; 
	}

	@Override
	public List<UsersRolesDto> listUserRoles() {
		//devuelve un tipo List UserRolesDto
		//vamos a hacer un mapeo para la fila. Se ocupa el rowMapper -->se ocupa clase BeanPropertyRowMapper, convierte una clase a un objeto, así
		//jdbcTemplate sabe que tiene que devolver un dato tipo UserRolesDto
		//como esta consulta no recibe argumentos dado que es un Select * ; por lo tanto se quita el "args".	
			List<UsersRolesDto> listUsersRoles= jdbcTemplate.query(listUsersRolesSQL, BeanPropertyRowMapper.newInstance(UsersRolesDto.class));
			return listUsersRoles;

}
}
