package cl.natacion.testing.model.DAO;

import java.util.List;

import cl.natacion.testing.model.DTO.CursosDto;

public interface CursosDao {
	public int insert(CursosDto cursosDto); //Es tipo int debido a que la operación devuelve la cantidad de filas modificadas, en este caso queremos realizar sólo una modificacion. Recibe un dato tipo SalesDTO con nombre SalesDTO
	public CursosDto get(int id_curso); //como es un read, vamos a obtener un dato de SalesDTO, un sólo sale y lo manejamos a nivel DTO,
	public int update(CursosDto cursosDto); //recibe un dato tipo SalesDTO con nombre SalesDTO
	public int delete(int id_curso);//
	public List<CursosDto> listCursos();// una lista de tipo SalesDTO llamado list
}
