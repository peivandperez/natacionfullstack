	package cl.natacion.testing.model.DTO;

public class AlumnosDto {

	 String nombre;
	 String apellido;
	 String mail;
	 String rut;
	 String activo; //valores 'Y' -'N'
	 String direccion;
	 String telefono;
	 String comuna;
	 
	 
	@Override
	public String toString() {
		return "AlumnosDto [nombre=" + nombre + ", apellido=" + apellido + ", mail=" + mail + ", rut=" + rut
				+ ", activo=" + activo + ", direccion=" + direccion + ", telefono=" + telefono + ", comuna=" + comuna
				+ "]";
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public AlumnosDto(String nombre, String apellido, String mail, String rut, String activo, String direccion,
			String telefono, String comuna) {
		this.nombre = nombre;
		this.apellido = apellido;
		this.mail = mail;
		this.rut = rut;
		this.activo = activo;
		this.direccion = direccion;
		this.telefono = telefono;
		this.comuna = comuna;
	}
	public AlumnosDto() {
	}
	 

	

}
