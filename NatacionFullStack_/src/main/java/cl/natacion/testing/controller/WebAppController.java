package cl.natacion.testing.controller;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Servlet implementation class WebAppController
 */
@Controller("/WebAppController")
public class WebAppController {
	
		@RequestMapping("/")
		public String getHome(){
			return "index";
		}
	
		//
		@RequestMapping("/login")
		public String getLogin(){
			return "login";
		}
		
		
		@RequestMapping("/HomeAdmin")
		public String getHomeAdmin(){
			return "HomeAdmin";
		}
		
		
		@RequestMapping("/index")
		public String getIndex(){
			return "index";
			
		}
		
		
		@RequestMapping("/postulacion")
		public String getPostulacion(){
			return "postulacion";
		}
		
		@RequestMapping("/RegistrarAlumno")
		public String getRegistrarAlumno(){
			return "RegistrarAlumno";
		}
		
		@RequestMapping("/nuestro_equipo")
		public String getNuestroEquipo(){
			return "nuestro_equipo";
		}
		
		@RequestMapping("/beneficios")
		public String getBeneficios(){
			return "beneficios";
		}
	
		@RequestMapping("/RegistrarCurso")
		public String getRegistrarCursos(){
			return "RegistrarCursos3";
		}
		
		
		@RequestMapping("/403")
		public String get403(){
			return "403";
		}

		
		

}
