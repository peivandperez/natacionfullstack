package cl.natacion.testing.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import cl.natacion.testing.service.PostulacionService;
import cl.natacion.testing.model.DTO.AlumnosDto;
import cl.natacion.testing.model.DTO.PostulacionDto;

@Controller
@RequestMapping(value="/postulacion")
public class PostulacionController {
	
	@Autowired
	PostulacionService postulacionService;
	
	@RequestMapping(value="/list")
	public @ResponseBody List<PostulacionDto> ajaxList(HttpServletRequest req, HttpServletResponse res) {
		List<PostulacionDto> list = postulacionService.list();
		return list;
	}
	
	
	@RequestMapping(value="/insert")
	public @ResponseBody int ajaxInsert(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			PostulacionDto postulacion = new Gson().fromJson(requestData, PostulacionDto.class);
			rows = postulacionService.insert(postulacion);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return rows;
	}

	@RequestMapping(value="/delete")
	public @ResponseBody int ajaxDelete(HttpServletRequest req,HttpServletResponse res){
		int rows=0;
		try {
			rows= postulacionService.delete(req.getParameter("rut"));
			System.out.println(req.getParameter("rut"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows;	
		}
}
