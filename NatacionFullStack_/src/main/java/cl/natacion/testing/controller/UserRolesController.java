package cl.natacion.testing.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.google.gson.Gson;

import cl.natacion.testing.model.DTO.UsersRolesDto;
import cl.natacion.testing.service.UsersRolesService;




@Controller
@RequestMapping(value="/userRoles")
public class UserRolesController {

	@Autowired
	UsersRolesService usersRolesService;
	
	@Autowired
	private PasswordEncoder bCryptPasswordEncoder;
	
	@RequestMapping(value="/list")
	public @ResponseBody List<UsersRolesDto> ajaxList(HttpServletRequest req, HttpServletResponse res) {
		List<UsersRolesDto> list = usersRolesService.list();
		return list;
	}
	
	@RequestMapping(value="/get")
	public @ResponseBody UsersRolesDto ajaxGet(HttpServletRequest req, HttpServletResponse res) {
		UsersRolesDto user = usersRolesService.get(Integer.parseInt(req.getParameter("id_users")));
		return user;
	}
	
	@RequestMapping(value="/delete")
	public @ResponseBody int ajaxDelete(HttpServletRequest req, HttpServletResponse res) {
		int rows = usersRolesService.delete(Integer.parseInt(req.getParameter("id_users")));
		return rows;
	}
	
	@RequestMapping(value="/insert")
	public @ResponseBody int ajaxInsert(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		int rowsRoles=0;
		
		UsersRolesDto usersRolesDto = new UsersRolesDto();
		
		String password = req.getParameter("password");
		usersRolesDto.setPassword(bCryptPasswordEncoder.encode(password));
		usersRolesDto.setUsername(req.getParameter("username"));
		usersRolesDto.setEnabled("Y");
		usersRolesDto.setRole(req.getParameter("role"));
		
		rows=usersRolesService.insert(usersRolesDto);
		rowsRoles=usersRolesService.insertRoles(usersRolesDto);
	
		return rows;
	}
	
	@RequestMapping(value="/update")
	public @ResponseBody int ajaxUpdate(HttpServletRequest req, HttpServletResponse res) {
		UsersRolesDto usersRolesDto = new UsersRolesDto();
		int rows=0;
		int rows2=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			UsersRolesDto users = new Gson().fromJson(requestData, UsersRolesDto.class);
			rows = usersRolesService.update(users);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		usersRolesDto.setId_roles((Integer.parseInt(req.getParameter("id_roles"))));
		rows2=usersRolesService.updateRoles(usersRolesDto);
	
	
		return rows;
	}
	
}
