package cl.natacion.testing.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import cl.natacion.testing.model.DTO.ClasesDto;
import cl.natacion.testing.service.ClasesService;

@Controller
@RequestMapping(value="/clases")
public class ClasesController {

	@Autowired
	ClasesService clasesService;
	
	
	@RequestMapping(value="/list")
	public @ResponseBody List<ClasesDto> ajaxList(HttpServletRequest req, HttpServletResponse res) {
		List<ClasesDto> list = clasesService.list();
		return list;
	}
	
	@RequestMapping(value="/get")
	public @ResponseBody ClasesDto ajaxGet(HttpServletRequest req, HttpServletResponse res) {
		ClasesDto clases = clasesService.get(Integer.parseInt(req.getParameter("id_clase")));
		return clases;
	}
	
	@RequestMapping(value="/delete")
	public @ResponseBody int ajaxDelete(HttpServletRequest req, HttpServletResponse res) {
		int rows = clasesService.delete(Integer.parseInt(req.getParameter("id_clase")));
		return rows;
	}
	
	@RequestMapping(value="/insert")
	public @ResponseBody int ajaxInsert(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
		    Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		    ClasesDto clases = gson.fromJson(requestData, ClasesDto.class);
			rows = clasesService.insert(clases);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return rows;
	}
	
	@RequestMapping(value="/update")
	public @ResponseBody int ajaxUpdate(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			
			System.out.println(requestData);
		    Gson gson=  new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		    ClasesDto clases = gson.fromJson(requestData, ClasesDto.class);
			rows = clasesService.update(clases);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return rows;
	}
	
}

	
	
	
	
	

