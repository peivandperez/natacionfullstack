package cl.natacion.testing.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import cl.natacion.testing.model.DTO.AlumnosDto;
import cl.natacion.testing.service.AlumnosService;




@Controller
@RequestMapping(value="/alumnos")
public class AlumnosController {

	@Autowired
	AlumnosService alumnosService;
	
	@RequestMapping(value="/list")
	public @ResponseBody List<AlumnosDto> ajaxList(HttpServletRequest req, HttpServletResponse res) {
		List<AlumnosDto> list = alumnosService.list();
		return list;
	}
	
	@RequestMapping(value="/get")
	public @ResponseBody AlumnosDto ajaxGet(HttpServletRequest req, HttpServletResponse res) {
		return alumnosService.get(req.getParameter("rut"));
		
	}
	
	
	
	@RequestMapping(value="/delete")
	public @ResponseBody int ajaxDelete(HttpServletRequest req,HttpServletResponse res){
		int rows=0;
		try {
			rows= alumnosService.delete(req.getParameter("rut"));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rows;	
		}
	
	@RequestMapping(value="/insert")
	public @ResponseBody int ajaxInsert(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			AlumnosDto alumno = new Gson().fromJson(requestData, AlumnosDto.class);
			rows = alumnosService.insert(alumno);
		} catch (IOException e) {
			e.printStackTrace();
		}
		//
	
		return rows;
	}
	
	@RequestMapping(value="/update")
	public @ResponseBody int ajaxUpdate(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			AlumnosDto alumno = new Gson().fromJson(requestData, AlumnosDto.class);
			rows = alumnosService.update(alumno);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return rows;
	}
	
}
