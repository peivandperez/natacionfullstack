package cl.natacion.testing.controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;


import cl.natacion.testing.model.DTO.ProfesoresDto;
import cl.natacion.testing.service.ProfesoresService;

@Controller
@RequestMapping(value="/profesores")
public class ProfesoresController {

	@Autowired
	ProfesoresService profesoresService;
	
	
	@RequestMapping(value="/list")
	public @ResponseBody List<ProfesoresDto> ajaxList(HttpServletRequest req, HttpServletResponse res) {
		List<ProfesoresDto> list = profesoresService.list();
		return list;
	}
	
	@RequestMapping(value="/get")
	public @ResponseBody ProfesoresDto ajaxGet(HttpServletRequest req, HttpServletResponse res) {
		ProfesoresDto profesores = profesoresService.get(Integer.parseInt(req.getParameter("id_profesor")));
		return profesores;
	}
	
	@RequestMapping(value="/delete")
	public @ResponseBody int ajaxDelete(HttpServletRequest req, HttpServletResponse res) {
		int rows = profesoresService.delete(Integer.parseInt(req.getParameter("id_profesor")));
		return rows;
	}
	
	@RequestMapping(value="/insert")
	public @ResponseBody int ajaxInsert(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			ProfesoresDto profesores = new Gson().fromJson(requestData, ProfesoresDto.class);
			rows = profesoresService.insert(profesores);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return rows;
	}
	
	
	
	@RequestMapping(value="/update")
	public @ResponseBody int ajaxUpdate(HttpServletRequest req, HttpServletResponse res) {
		int rows=0;
		try {
			String requestData = req.getReader().lines().collect(Collectors.joining());
			ProfesoresDto profesores = new Gson().fromJson(requestData, ProfesoresDto.class);
			rows = profesoresService.update(profesores);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		return rows;
	}
	
}
