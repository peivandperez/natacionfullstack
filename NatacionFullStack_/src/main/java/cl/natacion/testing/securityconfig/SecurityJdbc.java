package cl.natacion.testing.securityconfig;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityJdbc extends WebSecurityConfigurerAdapter {

	@Autowired
	private DataSource dataSource;

	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
		
		.antMatchers("/").permitAll() // This will be your home screen URL
		//.antMatchers("/resources/**").permitAll()
		.antMatchers("/img/home/landing_1.png").permitAll() 
		.antMatchers("/img/home/landing_2.png").permitAll() 
		.antMatchers("/img/home/landing_3.png").permitAll() 
		.antMatchers("/img/home/nosotros.jpg").permitAll() 
		.antMatchers("/img/home/beneficios.jpg").permitAll()
		.antMatchers("/js/postulacion.js").permitAll()
		.antMatchers("/js/postulacion.js").permitAll()
		.antMatchers("/nuestro_equipo").permitAll() 
		.antMatchers("/css/loginstyle.css").permitAll()
		.antMatchers("/css/style.css").permitAll()
		.antMatchers("/img/home/logo.jpg").permitAll() 
		.antMatchers("/favicon.ico").permitAll()
		.antMatchers("/postulacion").permitAll()
		.antMatchers("/postulacion/insert").permitAll()
		.antMatchers("/beneficios").permitAll()

		.antMatchers("/RegistrarAlumno").hasAuthority("admin")
		;
		
		http
		.authorizeRequests()
		.anyRequest().authenticated()
		.and()
		.formLogin()
		.loginPage("/login").permitAll() //todos pueden ingresar a la página login
			.and()
			.logout()
			.permitAll() //todos pueden des loggearse
			.logoutSuccessUrl("/index")
			
			
		; 
	
		http.exceptionHandling().accessDeniedPage("/403")
		;
	//hola
	}

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.jdbcAuthentication()
			.dataSource(dataSource)
			.passwordEncoder(passwordEncoder())
			.usersByUsernameQuery("SELECT username,password,enabled FROM users WHERE username=?")
			.authoritiesByUsernameQuery(
					"SELECT u.username, r.role " +
					"FROM users_roles ur, users u , roles r " +
					"WHERE u.id_users = ur.id_users and r.id_roles = ur.id_roles and u.username=?"
			);	
		
	}
		
		
	
	  @Bean
	    public PasswordEncoder passwordEncoder() {
	        return new BCryptPasswordEncoder();
	    }


	 
	
		
}
