package cl.natacion.testing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import cl.natacion.testing.model.DAO.AlumnosDao;
import cl.natacion.testing.model.DTO.AlumnosDto;

@Service
public class AlumnosServiceImpl implements AlumnosService {
	
	@Autowired
	AlumnosDao alumnosDao;

	public List<AlumnosDto> list() {
		return alumnosDao.listAlumnos();
	}
	
	public AlumnosDto get(String rut) {
		return alumnosDao.get(rut);
	}

	public int delete(String rut) {
		return alumnosDao.delete(rut);
	}
	
	public int insert(AlumnosDto alumnosDto) {
		return alumnosDao.insert(alumnosDto);
	}
	
	public int update(AlumnosDto alumnosDto) {
		return alumnosDao.update(alumnosDto);
	}

}
