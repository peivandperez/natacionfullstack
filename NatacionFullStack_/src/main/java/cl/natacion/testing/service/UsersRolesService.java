package cl.natacion.testing.service;

import java.util.List;

import cl.natacion.testing.model.DTO.UsersRolesDto;



public interface UsersRolesService {
	public List<UsersRolesDto> list();
	public UsersRolesDto get(int id_users);
	public int insert(UsersRolesDto usersRolesDto);
	public int update(UsersRolesDto usersRolesDto);
	public int delete(int id_users);
	
	public int insertRoles(UsersRolesDto userRolesDto);
	public int updateRoles(UsersRolesDto userRolesDto);
}
