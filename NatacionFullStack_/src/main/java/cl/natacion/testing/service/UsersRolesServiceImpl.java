package cl.natacion.testing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.natacion.testing.model.DAO.UsersRolesDao;
import cl.natacion.testing.model.DTO.UsersRolesDto;



@Service
public class UsersRolesServiceImpl implements UsersRolesService {

	@Autowired
	UsersRolesDao usersRolesDao;

	@Override
	public List<UsersRolesDto> list() {
		return usersRolesDao.listUserRoles();
	}

	@Override
	public UsersRolesDto get(int id_users) {
		return usersRolesDao.get(id_users);
	}

	@Override
	public int insert(UsersRolesDto usersRolesDto) {
		return usersRolesDao.insert(usersRolesDto);
	}

	@Override
	public int update(UsersRolesDto usersRolesDto) {
		return usersRolesDao.update(usersRolesDto);
	}

	@Override
	public int delete(int id_users) {
		return usersRolesDao.delete(id_users);
	}

	@Override
	public int insertRoles(UsersRolesDto usersRolesDto) {
	
		return usersRolesDao.insertRoles(usersRolesDto);
	}

	@Override
	public int updateRoles(UsersRolesDto userRolesDto) {
		return usersRolesDao.updateRoles(userRolesDto);
	}

	
}
