package cl.natacion.testing.service;

import java.util.List;

import cl.natacion.testing.model.DTO.ProfesoresDto;



public interface ProfesoresService {
	
	public List<ProfesoresDto> list();
	public ProfesoresDto get(int id_profesor);
	public int insert(ProfesoresDto profesoresDto);
	public int update(ProfesoresDto profesoresDto);
	public int delete(int id_profesor);
	
}


