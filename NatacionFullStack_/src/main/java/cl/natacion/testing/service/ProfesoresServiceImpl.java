package cl.natacion.testing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import cl.natacion.testing.model.DAO.ProfesoresDao;
import cl.natacion.testing.model.DTO.ProfesoresDto;

@Service
public class ProfesoresServiceImpl implements ProfesoresService {

	@Autowired
	ProfesoresDao profesoreaDao;
	
	public List<ProfesoresDto> list() {
		return profesoreaDao.listProfesores();
	}
	
	public ProfesoresDto get(int id_profesor) {
		return profesoreaDao.get(id_profesor);
	}

	public int delete(int id_profesor) {
		return profesoreaDao.delete(id_profesor);
	}
	
	public int insert(ProfesoresDto profesoresDto) {
		return profesoreaDao.insert(profesoresDto);
	}
	
	public int update(ProfesoresDto profesoresDto) {
		return profesoreaDao.insert(profesoresDto);
	}
}
