package cl.natacion.testing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.natacion.testing.model.DAO.ClasesDao;
import cl.natacion.testing.model.DTO.ClasesDto;

@Service
public class ClasesServiceImpl implements ClasesService {

	@Autowired
	ClasesDao clasesDao;
	
	@Override
	public List<ClasesDto> list() {
		return clasesDao.listClases();
	}

	@Override
	public ClasesDto get(int id_clase) {
		return clasesDao.get(id_clase);
	}

	@Override
	public int insert(ClasesDto clasesDto) {
		return clasesDao.insert(clasesDto);
	}

	@Override
	public int update(ClasesDto clasesDto) {
		return clasesDao.update(clasesDto);
	}
	
	@Override
	public int delete(int id_clase) {
		return clasesDao.delete(id_clase);
	}

}
