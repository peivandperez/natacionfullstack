package cl.natacion.testing.service;

import java.util.List;

import cl.natacion.testing.model.DTO.ClasesDto;

public interface ClasesService {
	public List<ClasesDto> list();
	public ClasesDto get(int id_clase);
	public int insert(ClasesDto ClasesDto);
	public int update(ClasesDto ClasesDto);
	public int delete(int id_clase);

}
