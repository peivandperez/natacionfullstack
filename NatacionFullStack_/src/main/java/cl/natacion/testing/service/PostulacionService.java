package cl.natacion.testing.service;


import java.util.List;

import cl.natacion.testing.model.DTO.PostulacionDto;
public interface PostulacionService {

	public List<PostulacionDto> list();
	public int insert(PostulacionDto postulacionDto);
	public int delete(String rut);

	
}
