package cl.natacion.testing.service;

import java.util.List;


import cl.natacion.testing.model.DTO.AlumnosDto;

public interface AlumnosService {
	
	public List<AlumnosDto> list();
	public AlumnosDto get(String rut);
	public int insert(AlumnosDto alumnosDto);
	public int update(AlumnosDto alumnosDto);
	public int delete(String rut);

}
