package cl.natacion.testing.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cl.natacion.testing.model.DAO.PostulacionDao;
import cl.natacion.testing.model.DTO.PostulacionDto;




@Service
public class PostulacionServiceImpl implements PostulacionService {

	@Autowired
	PostulacionDao postulacionDao;

	
	public int insert(PostulacionDto postulacionDto) {
		
		return postulacionDao.insert(postulacionDto);
	}


	@Override
	public List<PostulacionDto> list() {
		return postulacionDao.listPostulacion();
	}


	@Override
	public int delete(String rut) {
		return postulacionDao.delete(rut);
	}

}
